Las configuraciones de artefactos minetest
=====================


## Contexto de prueba de minas

Minetest tiene 4 tipos de partes:

* **motor**: el motor funciona con la biblioteca irrlicht, el motor es la parte que
maneja el juego y representa la lógica de los jugadores cuando se mueve e interactúa con
el juego.
* versión: el motor más antiguo (las versiones como 0.4 y 4.X) y más nuevo (los de 5.0 y 5.X+)
* sabor: está el minetest original, también multicraft, freeminer y mientest4/minetest5
* **mods**: son como complementos que mejoran el juego del motor utilizando la api minetest.
* **juegos**: son la unión de mods y archivos de configuración para proporcionar objetivos lógicos de juego específicos

## Archivos de configuración

El nombre del archivo `.conf` depende del tipo de contenido:

| archivo | contexto | versiones minetest | identificacion y significado |
| ------------------- | ------- | ----------------- | -------------------------- |
| `minetest.conf` | motor | todas las versiones | para ejecutar la configuración |
| `juego.conf` | juegos | 0.4, 5.X | para juegos en el motor. |
| `depende.txt` | mods | 0.4,5.1,5.2 | para paquetes de mods, solo la parte de dependencia |
| `descripción.txt` | mods | 0.4,5.1,5.2 | para mods, solo la parte de descripción |
| `mod.conf` | mods | 5.X | para mods en los juegos, depende y descripción unida |
| `modpack.txt` | mods | 0.4,5.0,5.1,5.2 | para la identificación de paquetes de mods de mods. |
| `modpack.conf` | mods | 5.X | para la identificación de paquetes de mods de mods. |
| `paquete_textura.conf` | juegos | 5.X | para paquetes de texturas. |

La siguiente sección explicará los dos tipos de archivos que puede configurar y dónde se aplican:

#### Archivos entendidos

Los archivos `txt` solo tienen una línea si son la descripción y una línea por nombre técnico de mods para cada uno depende.

Los archivos `.conf` usan un formato clave-valor, separados usando iguales. Aquí hay un ejemplo simple:

```
nombre = minenux
descripción = MinenuX
```

La siguiente sección explicará el contexto de los valores clave y dónde se aplican:

#### Valores entendidos

| clave | contexto | Descripción del valor |
| ------------------------------------- | ----------- | ------------------------ |
| `nombre` | mods, juegos | el nombre técnico del mod. |
| `descripción` | mods | Una breve descripción para mostrar en el cliente. |
| `juegos_soportados` | mods | Lista de nombres técnicos de juegos compatibles. |
| `juegos_no_compatibles` | mods | Lista de nombres técnicos de juegos no admitidos. Útil para anular la detección de soporte del juego. |
| `depende` | mods | Dependencias rígidas separadas por comas. |
| `opcional_depende` | mods | Dependencias blandas separadas por comas. |
| `min_minetest_version` | mods, juegos | La versión mínima de Minetest en la que se ejecuta, consulte [Versiones mínimas y máximas de Minetest](#min_max_versions). |
| `max_minetest_version` | mods, juegos | La versión máxima de Minetest en la que se ejecuta, consulte [Versiones mínimas y máximas de Minetest](#min_max_versions). |

El archivo `minetest.conf` tiene una enorme lista de claves para enumerar, que ya se describen en cada archivo de versión del motor.

##### .cdb.json para desarrolladores

Puede incluir un archivo `.cdb.json` en la raíz de su directorio de contenido (es decir, junto a un .conf)
para actualizar el paquete meta.

Debe ser un diccionario JSON con una o más de las siguientes claves opcionales:

* `tipo`: Uno de `JUEGO`, `MOD`, `TXP`.
* `título`: Título legible por humanos.
* `name`: Nombre técnico (necesita permiso si ya está aprobado).
* `breve_descripción`
* `dev_state`: uno de `WIP`, `BETA`, `ACTIVELY_DEVELOPED`, `MAINTENANCE_ONLY`, `TAL CUAL`, `DEPRECATED`, `BUSCANDO_MANTENEDOR`.
* `tags`: Lista de nombres de etiquetas
* `content_warnings`: Lista de nombres de advertencias de contenido
* `license`: un nombre de licencia pero para el código fuente
* `media_license`: Un nombre de licencia pero para archivos multimedia
* `long_description`: descripción de descuento larga.
* `repo`: URL del repositorio de Git.
* `sitio web`: URL del sitio web.
* `issue_tracker`: URL de seguimiento de problemas.
* `foros`: ID del tema del foro.
* `video_url`: URL de un vídeo.

Use `null` o `[]` para desarmar campos donde sea relevante.

Ejemplo:

```json
{
"título": "Foo bar",
"etiquetas": ["pvp", "supervivencia"],
"licencia": "MIT",
"sitio web": nulo
}
```

