# Como comenzar
===========================

This document will indicates you how to work and start to develop for 
and what to be used in the project. **Para español leer [DESARROLLO.md](DESARROLLO.md)**

* [How to start to work](#how-to-start-to-work)
 * [1 Requirements to work](#1-requirements-to-work)
 * [2 Setting up your environment](#2-setting-up-your-environment)
 * [3 clone the sources](#3-clone-the-sources)
 * [4 Upload to Geany and view](#4-upload-to-Geany-and-view)
* [Development Structure](#development-structure)
 * [Code and sources](#code-and-sources)
 * [How to work with git](#how-to-work-with-git)

This is a general repo for everything the minenux project works on,
including its repos on github or gitlab, is used to have a revision
general of everything done more coordinated. It is focused on
have members work, and not collaborators, if you want to collaborate
Please go to each specific repository. check [README.md](README.md)

## How to start to work
---------------------------

Create a `Devel` directory in home, change to it and clone the repo there, 
start and start the Geany editor.

All this is explained in detail below by parts

### 1 Requirements to work

* username must be `general` with `adduser general` for all our reports
* git (project and repository manager) `apt-get install git git-core giggle`
* geany (editor for php management as well as preview) `apt-get install geany geany-plugin-webhelper`
* minetestX (client) to test modules `apt-get install minetestx`
* curl (invoke urls) `apt-get install curl`

### 2 Setting up your environment

create the user and add it to the indicated groups

```
adduser --gecos "" --shell /bin/bash --create-home --home /home/general general

usermod -a -G video,audio,games,netdev,plugdev general
```

**IMPORTANT** log in as the new user, **this will be the user of
habitual use and must be the one that you must use** for all reports, issues 
and tests against our environment and repositories.

configure the git user and put the root of the Devel directory to use the project:

```
cd /home/general
mkdir Devel
cd Devel
git config --global status.submoduleSummary true
git config --global diff.submodule log
git config --global fetch.recurseSubmodules on-demand
```

in case you use another user copy all your files and rectify,
switch to the new user:

```
cp ./* /home/general
chown -R general:general /home/general
find /home/general/Devel/ -type f -exec chmod 664 {} ";"
find /home/general/Devel/ -type d -exec chmod 775 {} ";"
```

This is not 100% reliable and only do it if you need some previous files.

### 3 clone the sources

Use git to get the sources and start the geany IDE to encode,
as user `general` clone the sources in Devel from home:

```
mkdir -p ~/Devel
CD Devel
git clone --recursive http://codeberg.org/minenux/minenux.git
cd minenux
git pull
git submodule init
git submodule update --rebase
git submodule foreach git checkout master
git submodule foreach git pull
```

**IMPORTANT** Assuming you have access to the repo, if you are not a direct member
you should "fork" the repository.

Working directly on the `minenux` repo is not for contributors, it is
for direct members, and only this repo should be used to have
vision and global work and make massive update.


### 4 Upload to Geany and view

* open the geany
    * go to menu->tools->admin plugins
    * enable webhelper(web helper), treebrowser(tree viewer) and addons(added extras)
    * accept and test the web viewer (which reloads itself) below in the last tab below
* in the projects menu open, load the file `Devel/minenux/minenux.geany` and it will load the project
    * in the list select the project or the directory `~/Devel/minenux`

**IMPORTANT NOTE** This is assuming your username is called `general` and
if not, you should use this as the username and path.

* with a plain text editor open the file minenux.geany DO NOT OPEN WITH THE GEANY!!
* in the `base_path=` variable change the path of home `general` replace "general" with your linux user
* delete the entire files section if it exists and save the file

# Development Structure
============================

This is a general repo for everything the minenux project works on,
including its repos on github or gitlab, is used to have a revision
general of everything done more coordinated. It is focused on
have members work, and not collaborators, if you want to collaborate
Please go to each specific repository. check [README.md](README.md)

## Code and sources

The [mods](mods) directory contains most of the source code,
since the [games](games) directory are mostly the same mods
but included in each specific game, but only some are altered
according to the needs of each game.

While the original work of each mod may be gross, or even windosero
code or contributions of this style are not allowed and must be in
correct format and oriented to Linux routes and formats.

## How to work with git

The main "minenux" repository contains git submodules inside,
apart from own files.

**THEREFORE**: commits within a submodule are independent of the main git
1. first before committing changes check if there are from the main with fetch and pull
2. second commit and push submodules before committing and pushing main
3. then commit to main and push to main, everyone will be up to date

``` try
git fetch
git pull
git submodule init && git submodule update --rebase
git submodule foreach git checkout master
git submodule foreach git pull
editor file.new # (or open geany here and work)
git add <file.new> # add this new file changed in geany to the commit repo
git commit -a -m 'updated repo added <file.new> modifications'
git push
```

in the successionn of commands all work done in the submodules is brought and updates "its brand" in the main one,
After you have everything up to date, you will edit a new file and start

**NOTE** Geany must have the addons and filetree plugins loaded and activated
