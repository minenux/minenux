# Como comenzar
===========================

Este documento le indicara instrucciones de como comenzar desarrollar 
y que usar en este proyecto

* [Como comenzar a trabajar](#como-comenzar-a-trabajar)
 * [1 Requisitos para trabajar](#1-requisitos-para-trabajar)
 * [2 Configurar tu entorno](#2-configurar-tu-entorno)
 * [3 clonar las fuentes](#3-clonar-las-fuentes)
 * [4 Cargar en Geany y ver en web](#4-cargar-en-geany-y-ver-en-web)
* [Estructura de desarrollo](#estructura-de-desarrollo)
 * [Codigo y fuentes](#codigo-y-fuentes)
 * [Como trabajar git](#como-trabajar-git)


## Como comenzar a trabajar
---------------------------

Crear un directorio `Devel` en home, cambiarse a este y alli clonar el repo, iniciar y arrancar el editor Geany.

Todo esto se explica en detalle a continuacion por partes

### 1 Requisitos para trabajar

* username debe ser `general` con `adduser general` para todo nuestros reportes
* git (manejador de repositorio y proyecto) `apt-get install git git-core giggle`
* geany (editor para manejo php asi como ver el preview) `apt-get install geany geany-plugin-webhelper`
* minetestX (interprete) para probar los modulos `apt-get install minetestx`
* curl (invocar urls) `apt-get install curl`

### 2 Configurar tu entorno

crea el usuario y agregalo a los grupos indicados

```
adduser --gecos "" --shell /bin/bash --create-home --home /home/general general

usermod -a -G video,audio,games,netdev,plugdev general
```

**IMPORTANTE** logearse en el nuevo usuario, **este sera el usuario de 
uso habitual y debe ser el que se emplee** para todos los reportes, asuntos 
y pruebas contra nuestro entorno y repositorios.

configura el usuario git y coloca en la raiz de tu home el directorio Devel para usar el proyecto:

```
cd /home/general
mkdir Devel
cd Devel
git config --global status.submoduleSummary true
git config --global diff.submodule log
git config --global fetch.recurseSubmodules on-demand
```

en caso de que use otro usuario, copie todos sus archivos y rectifique, 
cambiese al nuevo usuario:

```
cp ./* /home/general
chown -R general:general /home/general/Devel
find /home/general/Devel/ -type f -exec chmod 664 {} ";"
find /home/general/Devel/ -type d -exec chmod 775 {} ";"
```

Esto no es 100% fiable y solo hagalo en caso de necesitar algunos archivos previos.

### 3 clonar las fuentes

Se usa git para tener las fuentes y se arranca el IDE geany para codificar, 
como usuario `general` clonar las fuentes en Devel de home:

``` 
mkdir -p ~/Devel
cd Devel
git clone --recursive http://codeberg.org/minenux/minenux.git
cd minenux
git pull
git submodule init
git submodule update --rebase
git submodule foreach git checkout master
git submodule foreach git pull
```

**IMPORTANTE** Asumiendo que ud tiene acceso al repo, si ud no es un miembro directo 
deberia realizar "fork" del repositorio.

Trabajar directo en el repo de `minenux` no es para colaboradores, es 
para los miembros directos, y solo debe ser usado este repo para tener 
vision y trabajo global y hacer update masivo.


### 4 Cargar en Geany y ver en web

* abrir el geany
    * ir a menu->herramientas->admincomplementos
    * activar webhelper(ayudante web), treebrowser(visor de arbol) y addons(añadidos extras)
    * aceptar y probar el visor web (que se recarga solo) abajo en la ultima pestaña de las de abajo
* en el menu proyectos abrir, cargar el archivo `Devel/minenux/minenux.geany` y cargara el proyecto
    * en la listado seleccionar el proyecto o el directorio `~/Devel/minenux`

**NOTA IMPORTANTE** esto es asumiendo que su usuario se llama `general` y 
si no es asi debe usar este como nombre y ruta de usuario.

* con un editor de texto plano abrir el archivo minenux.geany NO ABRIR CON EL GEANY!!
* en la variable `base_path=` cambiar la ruta de home `general` sustituya "general" por su usuario linux
* borrar toda la seccion files si existiese y salvar el archivo

# Estructura de desarrollo
===========================

Esto es un repo general para todo lo que el proyecto minenux trabaja, 
incluyendo sus repos en github o gitlab, se emplea para tener una revision 
general de todo lo realizado mas coordinadamente. Esta enfocado en 
que los miembros trabajen, y no en los colaboradores, si desea colaborar 
favor dirijase a cada repositorio en especifico.

## Codigo y fuentes

El directorio [mods](mods) contiene la mayor parte de codigos fuentes, 
ya que el directorio [games](games) son en su mayoria los mismos mods 
pero incluidos en cada juego especifico, pero solo algunso son alterados 
segun las necesidades de cada juego.

Si bien el trabajo original de cada mod puede ser asqueroso, o incluso windosero 
no se admite codigo o contribuciones de este estilo y deben ser en 
correcto formato y orientado a rutas y formatos Linux.

## Como trabajar con git

El repositorio principal "minenux" contine adentro submodulos git, 
aparte de archivos propios.

**POR ENDE**: los commits dentro de un submodulo son independientes del git principal
1. primero antes de acometer cambios revise si hay desde elprincipal con fetch y pull
2. segundo haga commit y push en los submodulos antes de hacer commit y push en el principal
3. despues haga commit en el principal y push hacia el principal, todos estaran al dia

``` bash
git fetch
git pull
git submodule init && git submodule update --rebase
git submodule foreach git checkout master
git submodule foreach git pull
editor archivo.nuevo # (o abres el geany aqui y trabajas)
git add <archivo.nuevo> # agregas este archivo nuevo cambiado en geany al repo para acometer
git commit -a -m 'actualizado el repo adicionado <archivo.nuevo> modificaciones'
git push
```

En la sucesion de comandos se trajo todo trabajo realizado en los submodulos y actualiza "su marca" en el principal, 
despues que tiene todo a lo ultimo se editar un archivo nuevo y se acomete

**NOTA** Geany debe tener los plugins addons y filetree cargados y activados

