The minetest artifacts configurations
=====================


## Minetest context

Minetest has 4 kind of parts:

* **engine**: the engine is powered by irrlicht library, the engine its the part that 
  handles the game and renders the logic of the players when moves and interact with 
  the game.
    * version: the older engine (those version like 0.4 and 4.X) and newer (those from 5.0 and 5.X+ )
    * flavour: there's the origial minetest, also multicraft, freeminer and mientest4/minetest5
* **mods**: are like plugins that enhances the engine gaming using the minetest api.
* **games**: are the junction of mods and configs files to provide specific game logic targets

## Config files

The filename of the `.conf` file depends on the content type:

| file                | context | minetest versions | identificacion and meaning |
| ------------------- | ------- | ----------------- | -------------------------- |
| `minetest.conf`     | engine  | all versions      | for running configuration |
| `game.conf`         | games   | 0.4, 5.X          | for games on the engine. |
| `depends.txt`       | mods    | 0.4,5.1,5.2       | for mod packs, only the dependency part |
| `description.txt`   | mods    | 0.4,5.1,5.2       | for mods, only the descripton part |
| `mod.conf`          | mods    | 5.X               | for mods in the games, depends and description joined |
| `modpack.txt`       | mods    | 0.4,5.0,5.1,5.2   | for identifiaction of mod packs of mods. |
| `modpack.conf`      | mods    | 5.X               | for identification of mod packs of mods. |
| `texture_pack.conf` | games   | 5.X               | for texture packs. |

Next section will explain the two kind of files you can setup and where apply:

#### Understood files

The `txt` files only has one line if are the description and one line per mods technical name for each depends.

The `.conf` files uses a key-value format, separated using equals. Here's a simple example:

```
	name = minenux
	description = MinenuX
```

Next section will explain the key values context and where apply:

#### Understood values

| key                    | context  | Description of value |
| ---------------------- | ----------- | ------------------------ |
| `name`                 | mods, games | the mod technical name. |
| `description`          | mods        | A short description to show in the client. |
| `supported_games`      | mods        | List of supported game technical names. |
| `unsupported_games`    | mods        | List of not supported game technical names. Useful to override game support detection. |
| `depends`              | mods        | Comma-separated hard dependencies. |
| `optional_depends`     | mods        | Comma-separated soft dependencies. |
| `min_minetest_version` | mods, games | The minimum Minetest version this runs on, see [Min and Max Minetest Versions](#min_max_versions). |
| `max_minetest_version` | mods, games | The maximum Minetest version this runs on, see [Min and Max Minetest Versions](#min_max_versions). |

The `minetest.conf` file has a huge list of keys to list, that are already described in each engine release file.

##### .cdb.json for developers

You can include a `.cdb.json` file in the root of your content directory (ie: next to a .conf)
to update the package meta.

It should be a JSON dictionary with one or more of the following optional keys:

* `type`: One of `GAME`, `MOD`, `TXP`.
* `title`: Human-readable title.
* `name`: Technical name (needs permission if already approved).
* `short_description`
* `dev_state`: One of `WIP`, `BETA`, `ACTIVELY_DEVELOPED`, `MAINTENANCE_ONLY`, `AS_IS`, `DEPRECATED`, `LOOKING_FOR_MAINTAINER`.
* `tags`: List of tag names
* `content_warnings`: List of content warning names
* `license`: A license name but for source code
* `media_license`: A license name but for media files
* `long_description`: Long markdown description.
* `repo`: Git repo URL.
* `website`: Website URL.
* `issue_tracker`: Issue tracker URL.
* `forums`: forum topic ID.
* `video_url`: URL to a video.

Use `null` or `[]` to unset fields where relevant.

Example:

```json
{
    "title": "Foo bar",
    "tags": ["pvp", "survival"],
    "license": "MIT",
    "website": null
}
```

